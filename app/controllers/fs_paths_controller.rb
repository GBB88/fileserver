class FsPathsController < ApplicationController
  attr_accessor :dir
  def directory
    @fs_user = FsUser.find(session[:id])
    session[:current_dir] = params[:dir]
  end
  
  def mkdir
    # we need a relative path to display to the user and an absolute one to actually read the files
    relDir = File.join(session[:current_dir].split("|"),params[:dirName])
    absDir = File.join(Rails.root,"public",relDir)
    
    fsPath = FsPath.new()
    fsPath.path_name = relDir
    fsPath.public = params[:rights] == "o"
    fsPath.user_id = session[:id]
    if fsPath.save
      Dir.mkdir(absDir,0700)
      redirect_to directory_path(session[:current_dir])
    else
      render plain: "error saving path "+fsPath.inspect
    end
  end
  
  def upload
    # instance of UploadedFile (or so...)
    uploadedFile = params[:file]
    
    # we need a relative path to display to the user and an absolute one to actually read the files
    relFilePath = File.join(session[:current_dir].split("|"),uploadedFile.original_filename)
    absFilePath = File.join(Rails.root,"public",relFilePath)
    fsPath = FsPath.new()
    fsPath.path_name = relFilePath
    fsPath.public = params[:rights] == "o"
    fsPath.user_id = session[:id]
    if fsPath.save
      # file is automatically closed at the end of the block
      File.open(absFilePath,"wb") do |file|
        file.write(uploadedFile.read)
      end
      redirect_to directory_path(session[:current_dir])
    else
      render plain: "error saving path "+fsPath.inspect
    end
  end
  
  def deleteDir
    relFilePath = File.join(session[:current_dir].split("|"),params[:dirname])
    doDelete(relFilePath)
    redirect_to directory_path(session[:current_dir])
  end
  
  def deleteFile
    relFilePath = File.join(session[:current_dir].split("|"),params[:filename]+"."+params[:filetype])
    doDelete(relFilePath)
    redirect_to directory_path(session[:current_dir])
  end
  
  private
    def doDelete(relFilePath)
      # we need a relative path to display to the user and an absolute one to actually read the files
      absFilePath = File.join(Rails.root,"public",relFilePath)
      # through the unique validation in the model, one path name only exits once, here raise exception if it doesn't
      fsPath = FsPath.find_by! path_name: relFilePath
      fsPath.destroy
      File.delete(absFilePath)
    end
end
