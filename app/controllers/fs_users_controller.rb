class FsUsersController < ApplicationController
  def new
    if (@fs_user == nil)
      @fs_user = FsUser.new
    end
    render "welcome/index"
  end
  
  def register
    @fs_user = FsUser.new(fs_user_params)
    @fs_user.save
    render "welcome/index"
    #redirect_to root_path
  end
  
  def login
    #puts "params: "+login_params
    #render plain: FsUser.where("email= ? and pw= ?",login_params[:email],login_params[:pw]).count
    @fs_user = FsUser.where("email= ? and pw= ?",login_params[:email],login_params[:pw]).first
    if(@fs_user != nil)
      session[:id] = @fs_user.id
      # redirect to the root path of the uploads using pipe instead of slash
      redirect_to directory_path("|uploads")
    else
      render plain: "could not find user"
    end
  end
  
  def logout
    session[:id] = nil
    redirect_to root_path
  end
  
  private
    def fs_user_params
      params.require(:fs_user).permit(:email, :fname, :lname, :pw, :pw_confirmation)
    end
    
    def login_params
      params.require(:fs_user).permit(:email, :pw)
    end
end
