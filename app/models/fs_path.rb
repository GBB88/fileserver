class FsPath < ActiveRecord::Base
  validates :path_name, uniqueness: true
end
