class FsUser < ActiveRecord::Base
  validates :email, presence: true, uniqueness: true
  validates :fname, presence: true
  validates :lname, presence: true
  validates :pw_confirmation, presence: true
  validates :pw, confirmation: true
end
