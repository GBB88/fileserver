class CreateFsUsers < ActiveRecord::Migration
  def change
    create_table :fs_users do |t|
      t.string :fname
      t.string :string
      t.string :lname
      t.string :pw
      t.string :email

      t.timestamps
    end
  end
end
