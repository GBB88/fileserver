class CreateFsPaths < ActiveRecord::Migration
  def change
    create_table :fs_paths do |t|
      t.text :path_name
      t.boolean :public
      t.integer :user_id

      t.timestamps
    end
  end
end
